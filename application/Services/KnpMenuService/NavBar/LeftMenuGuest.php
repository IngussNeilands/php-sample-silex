<?php

namespace Sample\Silex\Services\KnpMenuService\NavBar {

    use Sample\Silex\Services\KnpMenuService\KnpMenuInterface;

    class LeftMenuGuest implements KnpMenuInterface {

        public function getMenu(\Knp\Menu\MenuFactory $factory, \Silex\Translator $translator) {

            $menu = $factory->createItem('root');

            $menu->setChildrenAttribute('class', 'nav navbar-nav');

            $menu->addChild($translator->trans('navbar.about'), array('route' => 'about'));

            #$app['translator']->trans('About')
            return $menu;
        }

    }

}