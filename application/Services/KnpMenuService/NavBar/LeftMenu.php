<?php

namespace Sample\Silex\Services\KnpMenuService\NavBar {

    use Sample\Silex\Services\KnpMenuService\KnpMenuInterface;

    class LeftMenu implements KnpMenuInterface {

        public function getMenu(\Knp\Menu\MenuFactory $factory, \Silex\Translator $translator) {

            $menu = $factory->createItem('root');

            $menu->setChildrenAttribute('class', 'nav navbar-nav');

            $menu->addChild($translator->trans('navbar.about'), array('route' => 'about'));

            $menu->addChild('Sample DB', array('route' => 'sample_view'));

            $menu->addChild('Demo-DropDown', array(
                        'label' => 'DropDown Demo <span class="caret"></span>',
                        'extras' => array('safe_label' => true),
                        'uri' => 'dashboard',
                    ))
                    ->setAttribute('class', 'dropdown')
                    ->setLinkAttribute('data-toggle', 'dropdown')
                    ->setLinkAttribute('class', 'dropdown-toggle')
                    ->setChildrenAttribute('class', 'dropdown-menu')
                    ->setChildrenAttribute('role', 'menu');

            $menu['Demo-DropDown']->addChild('Demo', array('route' => 'demo'));

            $menu['Demo-DropDown']->addChild('divider')
                    ->setLabel('')
                    ->setAttribute('class', 'divider');

            $menu['Demo-DropDown']->addChild('Demo1', array('route' => 'demo_1'));
            $menu['Demo-DropDown']->addChild('Demo2', array('route' => 'demo_2'));
            $menu['Demo-DropDown']->addChild('Demo3', array('route' => 'demo_3'));

            return $menu;
        }

    }

}