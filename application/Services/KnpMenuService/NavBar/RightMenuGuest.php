<?php

namespace Sample\Silex\Services\KnpMenuService\NavBar {

    use Sample\Silex\Services\KnpMenuService\KnpMenuInterface;

    class RightMenuGuest implements KnpMenuInterface {

        public function getMenu(\Knp\Menu\MenuFactory $factory, \Silex\Translator $translator) {

            $menu = $factory->createItem('root');

            $menu->setChildrenAttribute('class', 'nav navbar-nav navbar-right');

            $menu->addChild('Login', array('route' => 'user_login'));

            return $menu;
        }

    }

}