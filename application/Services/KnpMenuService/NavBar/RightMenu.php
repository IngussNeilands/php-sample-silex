<?php

namespace Sample\Silex\Services\KnpMenuService\NavBar {

    use Sample\Silex\Services\KnpMenuService\KnpMenuInterface;

    class RightMenu implements KnpMenuInterface {

        public function getMenu(\Knp\Menu\MenuFactory $factory, \Silex\Translator $translator) {

            $menu = $factory->createItem('root');

            $menu->setChildrenAttribute('class', 'nav navbar-nav navbar-right');

            $menu->addChild('User Info', array('route' => 'user_info'));
            $menu->addChild('Logout', array('route' => 'user_logout'));

            return $menu;
        }

    }

}