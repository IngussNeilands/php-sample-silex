<?php

namespace Sample\Silex\Services\KnpMenuService {

    use Silex\Application;
    use Silex\ServiceProviderInterface;

    class KnpMenuServiceProvider implements ServiceProviderInterface {

        public function register(Application $app) {

            $app->register(new \Knp\Menu\Integration\Silex\KnpMenuServiceProvider());

            $app['knp_menu.voter'] = $app->share(function ($app) {
                $voter = new \Knp\Menu\Matcher\Voter\RouteVoter();
                $voter->setRequest($app['request']);
                return $voter;
            });

            $app['knp_menu.matcher.configure'] = $app->protect(function (\Knp\Menu\Matcher\Matcher $matcher) use ($app) {
                $matcher->addVoter($app['knp_menu.voter']);
            });
        }

        public function boot(Application $app) {

            $app['sample.knpmenu.navbar.left.guest'] = $app->share(function () use ($app) {

                $RightMenu = new NavBar\LeftMenuGuest();
                return $RightMenu->getMenu($app['knp_menu.factory'], $app['translator']);
            });

            $app['sample.knpmenu.navbar.left'] = $app->share(function () use ($app) {

                $RightMenu = new NavBar\LeftMenu();
                return $RightMenu->getMenu($app['knp_menu.factory'], $app['translator']);
            });

            $app['sample.knpmenu.navbar.right.guest'] = $app->share(function () use ($app) {

                $RightMenu = new NavBar\RightMenuGuest();
                return $RightMenu->getMenu($app['knp_menu.factory'], $app['translator']);
            });

            $app['sample.knpmenu.navbar.right'] = $app->share(function () use ($app) {

                $RightMenu = new NavBar\RightMenu();
                return $RightMenu->getMenu($app['knp_menu.factory'], $app['translator']);
            });

            $app['knp_menu.menus'] = array(
                'navbar.left.guest' => 'sample.knpmenu.navbar.left.guest',
                'navbar.left' => 'sample.knpmenu.navbar.left',
                'navbar.right.guest' => 'sample.knpmenu.navbar.right.guest',
                'navbar.right' => 'sample.knpmenu.navbar.right',
            );
        }

    }

}