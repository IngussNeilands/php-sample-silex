<?php

namespace Sample\Silex\Services\KnpMenuService {

    interface KnpMenuInterface {

        public function getMenu(\Knp\Menu\MenuFactory $factory, \Silex\Translator $translator);
    }

}