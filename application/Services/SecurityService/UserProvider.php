<?php

namespace Sample\Silex\Services\SecurityService {

    use Symfony\Component\Security\Core\User\UserProviderInterface;
    use Symfony\Component\Security\Core\User\UserInterface;
    use Symfony\Component\Security\Core\User\User;
    use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

    class UserProvider implements UserProviderInterface {

        protected $username = 'anonymous';
        protected $password = '';
        protected $users = array();

        public function __construct(array $security_users = array()) {

            $this->users = $security_users;
        }

        public function loadUserByUsername($username) {

            if (isset($this->users[$username]) && isset($this->users[$username][0]) && isset($this->users[$username][1]))
                return new User($username, $this->users[$username][1], array($this->users[$username][0]), true, true, true, true);
            
            return new User($this->username, $this->password, array('IS_AUTHENTICATED_ANONYMOUSLY'), true, true, true, true);
        }

        public function refreshUser(UserInterface $user) {

            if (!$user instanceof User)
                throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));

            return $this->loadUserByUsername($user->getUsername());
        }

        public function supportsClass($class) {

            return $class === 'Symfony\Component\Security\Core\User\User';
        }

    }

}
