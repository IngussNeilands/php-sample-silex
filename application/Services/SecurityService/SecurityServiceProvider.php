<?php

namespace Sample\Silex\Services\SecurityService {

    use Silex\Application;
    use Silex\ServiceProviderInterface;

    class SecurityServiceProvider implements ServiceProviderInterface {

        public function register(Application $app) {

            $app->register(new \Silex\Provider\SecurityServiceProvider());

            $app->register(new \Silex\Provider\SessionServiceProvider());

            $app['security.firewalls'] = [
                'secured' => [
                    'pattern' => '^/*',
                    'anonymous' => 'true',
                    'form' => [
                        'check_path' => '/user/login/check',
                        'login_path' => 'user_login',
                        'target_path_parameter' => 'true',
                        'use_referer' => 'true',
                    ],
                    'logout' => [
                        'logout_path' => '/user/logout',
                        'invalidate_session' => 'true'
                    ],
                    'users' => $app->share(function () use ($app) {
                        return new \Sample\Silex\Services\SecurityService\UserProvider($app['security_users']);
                    }),
                ],
            ];

            $app['security.encoder.digest'] = $app->share(function ($app) {
                return new \Symfony\Component\Security\Core\Encoder\PlaintextPasswordEncoder(true);
            });

            $app['security.role_hierarchy'] = $app->share(function ($app) {
                return $app['security_role_hierarchy'];
            });

            $app['security.access_rules'] = $app->share(function ($app) {
                return $app['security_access_rules'];
            });
        }

        public function boot(Application $app) {
            
        }

    }

}