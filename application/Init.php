<?php

require dirname(__DIR__) . '/vendor/autoload.php';

$config = \Symfony\Component\Yaml\Yaml::parse(file_get_contents(dirname(__DIR__) . '/config/config.yml'));

if (isset($_SERVER['VAGRANT']) || null !== getenv('VAGRANT')):

    $config_dev = \Symfony\Component\Yaml\Yaml::parse(file_get_contents(dirname(__DIR__) . '/config/config.dev.yml'));

    foreach ($config_dev as $config_key => $config_value)
        if (empty($config[$config_key]))
            $config[$config_key] = $config_value;
        else
            $config[$config_key] = is_array($config_value) ?
                    array_merge((array) $config[$config_key], $config_value) : $config_value;
endif;

return new \Sample\Silex\Application($config);
