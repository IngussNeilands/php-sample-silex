<?php

namespace Sample\Silex\Forms {

    use Sample\Silex\Models\SampleModelController;
    use Symfony\Component\Form\FormFactory;

    class SampleFormController {

        const SAMPLE_FORM_BUILDER = 'Sample\Silex\Forms\SampleForm';

        protected $sample_model_controller;
        protected $form_factory;

        public function __construct(SampleModelController $sample_model_controller, FormFactory $form_factory) {

            $this->sample_model_controller = $sample_model_controller;
            $this->form_factory = $form_factory;
        }

        public function get_form_by_id($sample_id) {

            return $this->form_factory->createBuilder(self::SAMPLE_FORM_BUILDER, $this->sample_model_controller->get_sample_by_id($sample_id))->getForm();
        }

        public function get_new_form() {

            return $this->form_factory->createBuilder(self::SAMPLE_FORM_BUILDER, $this->sample_model_controller->get_new_sample())->getForm();
        }

    }

}