<?php

namespace Sample\Silex\Forms {

    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\Form\Extension\Core\Type;
    use Symfony\Component\Form\AbstractType;

    class SampleForm extends AbstractType {

        public function buildForm(FormBuilderInterface $builder, array $options) {

            $builder->add('text', Type\TextType::class, array('label' => 'Text'));
        }

    }

}