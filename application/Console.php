<?php

set_time_limit(0);

$app = require dirname(__DIR__) . '/application/Init.php';

$app->boot();

$console = new \Symfony\Component\Console\Application('Silex Sample', 'n/a');

$console->addCommands(array(
    new \Sample\Silex\ConsoleCommands\GreetCommand(),
));

$console->run();
