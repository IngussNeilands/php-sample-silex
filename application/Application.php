<?php

namespace Sample\Silex {

    use Silex\Application as SilexApplication;

    class Application extends SilexApplication {

        public function __construct(array $values = array()) {

            parent::__construct($values);

            $app = $this;

            if (true == $app['debug'])
                \Symfony\Component\Debug\Debug::enable();

            $app->register(new \Sample\Silex\Services\SecurityService\SecurityServiceProvider());
            $app->register(new \Silex\Provider\UrlGeneratorServiceProvider());
            $app->register(new \Silex\Provider\TranslationServiceProvider());
            $app->register(new \Silex\Provider\ValidatorServiceProvider());
            $app->register(new \Silex\Provider\FormServiceProvider());
            $app->register_activerecord_db($app);

            $app['translator'] = $app->share($app->extend('translator', function($translator, $app) {

                        $translator->addLoader('yaml', new \Symfony\Component\Translation\Loader\YamlFileLoader());

                        foreach ($app['locale_translate'] as $language => $source)
                            $translator->addResource('yaml', dirname(__DIR__) . $source, $language);

                        return $translator;
                    }));

            $app->register(new \Silex\Provider\TwigServiceProvider(), [
                'twig.path' => dirname(__DIR__) . '/application/Templates',
                'twig.options' => array('debug' => $app['debug']),
            ]);
            $app->register(new \Sample\Silex\Services\KnpMenuService\KnpMenuServiceProvider());


            $app['routes'] = $app->extend('routes', function (\Symfony\Component\Routing\RouteCollection $routes) use ($app) {

                $loader = new \Symfony\Component\Routing\Loader\YamlFileLoader(
                        new \Symfony\Component\Config\FileLocator(dirname(__DIR__) . '/config')
                );

                $routes->addCollection($loader->load('routes.yml'));

                return $routes;
            });

            $app->before(function () use ($app) {

                if ($locale = $app['request']->get('locale'))
                    $app['locale'] = $locale;

                elseif ($locale = $app['request']->get('_locale'))
                    $app['locale'] = $locale;
            });
        }

        protected function register_activerecord_db(SilexApplication $app) {

            $app->register(new \Ruckuus\Silex\ActiveRecordServiceProvider(), array(
                'ar.default_connection' => $app['default.db.connection'],
                'ar.model_dir' => dirname(__DIR__) . '/application/Models',
            ));

            \ActiveRecord\Connection::$datetime_format = 'Y-m-d H:i:s';

            $app['db.sample.model'] = $app->share(function() {

                return new \Sample\Silex\Models\SampleModelController();
            });

            $app['db.sample.form'] = $app->share(function() use($app) {

                return new \Sample\Silex\Forms\SampleFormController($app['db.sample.model'], $app['form.factory']);
            });
        }

    }

}
