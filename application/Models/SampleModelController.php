<?php

namespace Sample\Silex\Models {
    
    use Sample\Silex\Models\Sample;

    class SampleModelController {

        public function has_sample_by_id($sample_id) {

            if (false == $this::get_sample_by_id($sample_id))
                return false;

            return true;
        }

        public function get_sample_by_id($sample_id) {

            return Sample::find_by_id($sample_id);
        }

        public function get_all_samples() {

            return Sample::find('all');
        }

        public function get_new_sample() {

            return new Sample;
        }

    }

}
