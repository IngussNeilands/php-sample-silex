<?php

namespace Sample\Silex\Controllers {

    use Silex\Application;

    class UserController {

        public function get_user_info(Application $app) {

            return $app['twig']->render('user/page.info.twig', array(
                        'security_role_hierarchy' => $app['security.role_hierarchy']
            ));
        }

        public function get_user_login(Application $app) {

            if ($app['security.authorization_checker']->isGranted('ROLE_USER'))
                return $app->redirect($app['url_generator']->generate('user_info'));

            return $app['twig']->render('user/page.login.twig', array(
                        'error' => $app['security.last_error']($app['request']),
                        'last_username' => $app['session']->get('_security.last_username'),
            ));
        }

        public function get_user_logout(Application $app) {
        }

    }

}
