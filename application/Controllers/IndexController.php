<?php

namespace Sample\Silex\Controllers {

    use Silex\Application;

    class IndexController {

        public function redirect_to_about(Application $app) {

            return $app->redirect($app['url_generator']->generate('about'));
        }

        public function get_about(Application $app) {

            #if ($app['security.authorization_checker']->isGranted('ROLE_USER'))
            #    return $app->redirect($app['url_generator']->generate('user'));

            return $app['twig']->render('page.about.twig', ['message' => 'Hello World!']);
        }

    }

}
