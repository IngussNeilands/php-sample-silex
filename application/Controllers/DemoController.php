<?php

namespace Sample\Silex\Controllers {

    use Silex\Application;

    class DemoController {

        public function get_demo_page(Application $app) {

            if (false == $app['security.authorization_checker']->isGranted('ROLE_USER'))
                return $app->redirect($app['url_generator']->generate('user_login'));

            return $app->redirect($app['url_generator']->generate('demo_1'));
        }

        public function get_demo_1_page(Application $app) {

            return $app['twig']->render('demo/page.demo.1.twig');
        }

        public function get_demo_2_page(Application $app) {

            return $app['twig']->render('demo/page.demo.2.twig');
        }

        public function get_demo_3_page(Application $app) {

            return $app['twig']->render('demo/page.demo.3.twig');
        }

    }

}
