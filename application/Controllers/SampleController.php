<?php

namespace Sample\Silex\Controllers {

    use Silex\Application;

    class SampleController {

        const ERROR_SAVE = 'Sample Data is not Valid! Save error;';

        public function get_view_page(Application $app) {

            return $app['twig']->render('sample/page.sample.view.twig', array(
                        'sample_view' => $app['db.sample.model']->get_all_samples(),
            ));
        }

        public function get_edit_page($sample_id, Application $app) {

            if (false == $app['db.sample.model']->has_sample_by_id($sample_id))
                return $app->redirect($app['url_generator']->generate('sample_view'));

            return $app['twig']->render('sample/page.sample.edit.twig', array(
                        'sample_form' => $app['db.sample.form']->get_form_by_id($sample_id)->createView(),
                        'sample_id' => $sample_id,
            ));
        }

        public function post_edit_page($sample_id, Application $app) {

            if (false == $app['db.sample.model']->has_sample_by_id($sample_id))
                return $app->redirect($app['url_generator']->generate('sample_view'));

            $sample_form = $app['db.sample.form']->get_form_by_id($sample_id);

            $sample_form->handleRequest($app['request']);

            if ($sample_form->isValid() && $sample_form->getData()->is_valid()):

                $sample_form->getData()->save();

                return $app->redirect($app['url_generator']->generate('sample_view'));

            endif;

            $app['session']->getFlashBag()->add('message', self::ERROR_SAVE);

            return $app->redirect($app['url_generator']->generate('sample_edit', array('sample_id' => $sample_id)));
        }

        public function get_new_page(Application $app) {

            return $app['twig']->render('sample/page.sample.new.twig', array(
                        'sample_form' => $app['db.sample.form']->get_new_form()->createView(),
            ));
        }

        public function post_new_page(Application $app) {

            $sample_form = $app['db.sample.form']->get_new_form();

            $sample_form->handleRequest($app['request']);

            if ($sample_form->isValid() && $sample_form->getData()->is_valid()):

                $sample_form->getData()->save();

                return $app->redirect($app['url_generator']->generate('sample_view'));

            endif;

            $app['session']->getFlashBag()->add('message', self::ERROR_SAVE);

            return $app->redirect($app['url_generator']->generate('sample_new'));
        }

        public function get_delete_page($sample_id, Application $app) {

            $sample = $app['db.sample.model']->get_sample_by_id($sample_id);

            if (isset($sample))
                $sample->delete();

            return $app->redirect($app['url_generator']->generate('sample_view'));
        }

    }

}
