<?php

$app = require __DIR__ . '/application/Init.php';

$environments = $app['phinx_migration']['environments'];
$environments['default_database'] = $app['default.db.connection'];
$environments['default_migration_table'] = 'db_version';

return array(
    'paths' => array('migrations' => __DIR__ . '/migrations'),
    'environments' => $environments,
);
