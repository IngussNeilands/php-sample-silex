<?php

use Phinx\Migration\AbstractMigration;

class CreateSampleTable extends AbstractMigration {

    /**
     * Migrate Up.
     */
    public function up() {

        $users = $this->table('samples');

        $users->addColumn('text', 'string', array('limit' => 20));

        $users->addColumn('updated_at', 'datetime');

        $users->addColumn('created_at', 'datetime');

        $users->save();
    }

    /**
     * Migrate Down.
     */
    public function down() {
        
    }

}
